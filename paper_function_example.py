from paper_function import *
import matplotlib.pyplot as plt
import numpy as np


t0 = 0.0
tf = 24.0
d=150.0
D=50.0
q=0.2
sigma_vv=0.001
sigma_vu=sigma_vv/q
N=1000
dT=20
ptype='IP'
theta=0.0
phi=0.0
genOrbit=False
orbName="chief_OTM_Orbit_7_1.2_4"
orbPath="case_1_2"

theta_list = [0.0, 90.0]
phi_list = [0.0]

results_list = []
for phi_deg in phi_list:
    for theta_deg in theta_list:

        time,pc = pc_profile(t0, tf, d=d, D=D, q=q, 
                             sigma_vv=sigma_vv, N=N, dT=dT, 
                             ptype=ptype,
                             theta=theta_deg, 
                             phi=phi_deg,
                             genOrbit=genOrbit,
                             orbName=orbName, 
                             orbPath=orbPath,
                             comt=True)

        row_dict = {
                    'T' : tf,
                    'd' : d,
                    'D' : D,
                    'N' : N,
                    'q' : q,
                    'sigma_vv' : sigma_vv,
                    'sigma_vu' : sigma_vu,
                    'theta' : theta_deg,
                    'phi' : phi_deg,
                    'time' : time,
                    'probability' : pc[0],
                    'prob_inst' : pc[1]
                    }
        results_list.append(row_dict)
        plt.plot(time, pc[0])
        if ptype == "IP": plt.plot(time, pc[1])

plt.show()

