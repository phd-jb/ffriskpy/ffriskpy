# -*- coding: utf-8 -*-

from mubody.mission import Mission
import numpy as np
import matplotlib.pyplot as plt
from astropy_healpix import HEALPix
from tqdm.auto import tqdm
import scipy as sc
import math
from sympy import *




def sphToCart(rthetaphi):
    #takes list rthetaphi (single coord)
    r       = rthetaphi[0]
    theta   = np.radians(rthetaphi[1]) # to radian
    phi     = np.radians(rthetaphi[2]) # to radian
    x = r * np.sin( theta ) * np.cos( phi )
    y = r * np.sin( theta ) * np.sin( phi )
    z = r * np.cos( theta )
    return np.array([x,y,z])

def cartToSph(xyz):
    #takes list xyz (single coord)
    x       = xyz[0]
    y       = xyz[1]
    z       = xyz[2]
    r       =  np.sqrt(x*x + y*y + z*z)
    theta   =  np.degrees(np.acos(z/r)) #to degrees
    phi     =  np.degrees(np.atan2(y,x)) #to degrees
    return np.array([r,theta,phi])


def rotation_matrix_calculation_xyzToUvw(rthetaphi):
    #takes list rthetaphi (single coord)
    #r       = rthetaphi[0]
    theta   = np.radians(rthetaphi[1]) # to radian
    phi     = np.radians(rthetaphi[2]) # to radian
    rotation_matrix_xyzToUvw = np.array([
            [ np.sin( theta ) * np.cos( phi ),  np.sin( theta ) * np.sin( phi ),   np.cos( theta )],
            [ np.cos( theta ) * np.cos( phi ),  np.cos( theta ) * np.sin( phi ),  -np.sin( theta )],
            [                  -np.sin( phi ),                    np.cos( phi ),               0.0]])
    return rotation_matrix_xyzToUvw



def print_ellipsoid(fig_number,
                    deputy_relative_vel_xyz,
                    deputy_relative_position_xyz,
                    theta_deg,
                    phi_deg):
    
    fig = plt.figure(fig_number,figsize=(10,7.5))
    ax = fig.add_subplot(111, projection='3d')
    X=deputy_relative_vel_xyz[:][0]
    Y=deputy_relative_vel_xyz[:][1]
    Z=deputy_relative_vel_xyz[:][2]
    
    ax.scatter(X,Y,Z, c='b', marker='o')
    
    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0

    mid_x = (X.max()+X.min()) * 0.5
    mid_y = (Y.max()+Y.min()) * 0.5
    mid_z = (Z.max()+Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    
    ax.set_title("Relative speed ellipsoid for theta "+str(theta_deg)+" and phi "+str(phi_deg))
    ax.set_xlabel("X [m]")
    ax.set_ylabel("Y [m]")
    ax.set_zlabel("Z [m]")
    
    x = [0.0, deputy_relative_position_xyz[0]]
    y = [0.0, deputy_relative_position_xyz[1]]
    z = [0.0, deputy_relative_position_xyz[2]]
    ax.scatter(x, y, z, c='red', s=100)
    ax.plot(x, y, z, color='black')
    
    return


def propag_deputy_and_analysis_col(deputy_rel_pos_xyz,
                                   deputy_rel_vel_xyz,
                                   aTSimulation_s,
                                   t_chief_orbit_position_s,
                                   chief,
                                   IC_chief,
                                   dCol_m,
                                   N,
                                   specific_prob):   
    
    #Collision state for each deputy velocity (distance between deputy and chief is less or equal to dCol_m)
    #aCollisionN = np.zeros(N)
    
    #Collision probability by time
    aCollisionT = np.zeros(len(aTSimulation_s))
    aCollisionSpecT = np.zeros(len(aTSimulation_s))
    
    deputy = Mission(mission_time=aTSimulation_s[-1])
    IC_frame = chief.frame
    
    
    for i in range(0,N,1):
        
        if (i%50) == 0: print("       N iter: "+str(i))
        
        #Vector de estado inicial de deputy (vector de estado del chief + offset)
        IC_deputy = IC_chief + np.array([   deputy_rel_pos_xyz[0],    deputy_rel_pos_xyz[1],    deputy_rel_pos_xyz[2],
                                         deputy_rel_vel_xyz[0][i], deputy_rel_vel_xyz[1][i], deputy_rel_vel_xyz[2][i]  ])
        IC_deputy = IC_deputy.reshape(-1,1)
        
        #Propagación del deputy para el tiempo mission_time
        deputy.IC(IC=IC_deputy, IC_frame=IC_frame, bar=False)
    
        #Calculo de la distancia en cada instante de tiempo (time) entre chief y deputy
        temp = np.zeros(len(aTSimulation_s))
        tempPSpec = np.zeros(len(aTSimulation_s))
        first_time = True
        for j in range(0,len(aTSimulation_s),1):
            separation = np.linalg.norm(chief.sat.r(t_chief_orbit_position_s + aTSimulation_s[j]) - deputy.sat.r(aTSimulation_s[j]), axis=0)
            if separation <= dCol_m:
                #aCollisionN[i] = 1
                if specific_prob:
                    if first_time: temp[j:] = 1; first_time = False
                    tempPSpec[j] = 1
                else:
                    temp[j:] = 1
                    break
        aCollisionT = aCollisionT + temp
        aCollisionSpecT = aCollisionSpecT + tempPSpec
    
    #pColN = aCollisionN.mean()
    pColT = aCollisionT[:]/N
    pColTSpec = aCollisionSpecT[:]/N
    
    return pColT, pColTSpec
    #return pColN, pColT, pColTSpec
    



#Generar orbita
#parametors
orb_rev = 1.2
mt = orb_rev*180*86400
n_seg = math.trunc(orb_rev*6)
ite = 4

deputy = Mission()

chief = Mission()
chief.load("chief_OTM_Orbit_7_1.2_4", "case_1_2")

#nombre_directorio
dir_name = 'case_1_2'

#Contador de figuras
fig_number = 0

#control
generate = False

if generate:
    chief.OTM(N_segments=n_seg, opt_iterations=ite)
    chief.save("chief_OTM_Orbit_"+str(n_seg)+"_"+str(orb_rev)+"_"+str(ite), dir_name=dir_name)
else:
    chief.load("chief_OTM_Orbit_7_1.2_4", dir_name)

# chief_orbit (Ax,Az,phi_xy, phi_z)

t_chief_orbit_position_s = 7510000.0 #to be defined

chief_cond = chief.sat.s(t_chief_orbit_position_s)

tSimulation_d = 1.0
tSimulation_s = tSimulation_d * (24.0*3600.0) #to be defined

k_array = np.array([5])
sigma_u_array = np.array([0.005])
N_array = np.array([1000])

d_m_array = np.array([150.0])
theta_deg_array = np.array([0.0, 90.0])
phi_deg_array = np.array([0.0, 90.0])

dCol_m = 50.0 #distance to consider a collision

specific_prob = True #Activation of Specific probability calculation


mean = np.array([0.0, 0.0, 0.0])


#Intervalo de tiempo en el que se van a analizar las diferencias entre trayectorias de chief y deputy [s]
dt_s = 20
#Número de intervalos según la duración de cada intervalo (dt_s) y el tiempo total a analizar
nInterTSimul = math.ceil(tSimulation_s/dt_s)
#Array de tiempo para analizar durante la propagación del deputy
aTSimulation_s = np.linspace(0, tSimulation_s ,nInterTSimul)



lPCollisionByD = []
lPCollisionTByD = []
lPCollisionSpecTByD = []

for phi_deg in phi_deg_array:
    
    print(" phi_deg:"+str(phi_deg))
    for theta_deg in theta_deg_array:
        
        print("  theta_deg:"+str(theta_deg))        
        for d_m in d_m_array:
            
            print("   d:"+str(d_m))
            deputy_relative_position_sph = np.array([d_m, theta_deg, phi_deg])
            
            rotation_matrix_xyzToUvw = rotation_matrix_calculation_xyzToUvw(deputy_relative_position_sph)
            rotation_matrix_uvwToXyz = np.transpose(rotation_matrix_xyzToUvw)
            
            #deputy_relative_position_uvw = np.array([u, v, w])
    
            deputy_relative_position_xyz = sphToCart(deputy_relative_position_sph)
            deputy_position_xyz = chief_cond[0:3] + deputy_relative_position_xyz

            for sigma_u in sigma_u_array:
                
                print("    sigma_u:"+str(sigma_u))
                
                for N in N_array:
                    
                    print("     N:"+str(N))
                    
                    aPCollisionByK = np.zeros(len(k_array))
                    lPCollisionTByK = []
                    lPCollisionSpecTByK = []
                    ind = 0
                    for k in k_array:
                
                        ind += 1
                        print("      k:"+str(k))
                        kf = np.float64(k)
                        cov_matrix_uvw = np.array([[pow(sigma_u,2),               0.0,               0.0],
                                                   [           0.0, pow(sigma_u/kf,2),               0.0],
                                                   [           0.0,               0.0, pow(sigma_u/kf,2)]])
                        
                        #Covariance matrix for xyz frame (C' = R C RT)
                        #cov_matrix_xyz = np.dot(np.dot(rotation_matrix_xyzToUvw, 
                        #                               cov_matrix_uvw),
                        #                        rotation_matrix_uvwToXyz)
                        cov_matrix_xyz = np.dot(np.dot(rotation_matrix_uvwToXyz, 
                                                       cov_matrix_uvw),
                                                rotation_matrix_xyzToUvw)
                        
                        
                        deputy_relative_vel_xyz = np.random.multivariate_normal(mean, cov_matrix_xyz, N).T
                        
                        #aPCollisionByK[ind-1], aPCollisionT
                        aPCollisionT, aPCollisionSpecT = propag_deputy_and_analysis_col(deputy_relative_position_xyz,
                                                                                             deputy_relative_vel_xyz,
                                                                                             aTSimulation_s,
                                                                                             t_chief_orbit_position_s,
                                                                                             chief,
                                                                                             chief_cond,
                                                                                             dCol_m,
                                                                                             N,
                                                                                             specific_prob)
                        lPCollisionTByK.append(aPCollisionT)
                        lPCollisionSpecTByK.append(aPCollisionSpecT)
                        
                        #fig_number += 1
                        #print_ellipsoid(fig_number,
                        #                deputy_relative_vel_xyz,
                        #                deputy_relative_position_xyz,
                        #                theta_deg,
                        #                phi_deg)
                
                    #lPCollisionByD.append(aPCollisionByK)
                    lPCollisionTByD.append(lPCollisionTByK)
                    lPCollisionSpecTByD.append(lPCollisionSpecTByK)


fig_number += 1
plt.figure(fig_number,figsize=(10,7.5))
#plt.plot(lPCollisionTByD[0][0][:],c='b',ls='-',label='k=4')
#plt.plot(lPCollisionTByD[0][1][:],c='r',ls='-',label='k=7')
#plt.plot(lPCollisionTByD[0][2][:],c='k',ls='-',label='k=10')
#plt.plot(lPCollisionTByD[0][0][:],c='b',ls='-',label='d=150m')
#plt.plot(lPCollisionTByD[1][0][:],c='r',ls='-',label='d=300m')
#plt.plot(lPCollisionTByD[2][0][:],c='k',ls='-',label='d=450m')
#plt.plot(lPCollisionTByD[0][0][:],c='b',ls='-',label='sigma_u=0.005')
#plt.plot(lPCollisionTByD[0][1][:],c='r',ls='-',label='sigma_u=0.010')
#plt.plot(lPCollisionTByD[0][2][:],c='k',ls='-',label='sigma_u=0.015')
#plt.plot(lPCollisionTByD[0][0][:],c='b',ls='-',label='N=100')
#plt.plot(lPCollisionTByD[0][1][:],c='r',ls='-',label='N=300')
#plt.plot(lPCollisionTByD[0][2][:],c='g',ls='-',label='N=500')
#plt.plot(lPCollisionTByD[0][3][:],c='m',ls='-',label='N=700')
#plt.plot(lPCollisionTByD[0][4][:],c='k',ls='-',label='N=1000')
plt.plot(aTSimulation_s,lPCollisionTByD[0][0][:],c='b',ls='-',label='phi=0, theta=0')
plt.plot(aTSimulation_s,lPCollisionTByD[1][0][:],c='r',ls='-',label='phi=0, theta=90')
plt.plot(aTSimulation_s,lPCollisionTByD[2][0][:],c='g',ls='-',label='phi=90, theta=0')
plt.plot(aTSimulation_s,lPCollisionTByD[3][0][:],c='k',ls='-',label='phi=90, theta=90')
plt.legend(loc=4)
plt.xlabel('Simulation time [s]')
plt.ylabel('Collision probability [-]')
plt.title('Collision probability by deputy initial position for N='+str(N), size=12)

fig_number += 1
plt.figure(fig_number,figsize=(10,7.5))
plt.plot(aTSimulation_s,lPCollisionTByD[0][0][:],c='b',ls='-',label='Collision Probability')
plt.plot(aTSimulation_s,lPCollisionSpecTByD[0][0][:],c='r',ls='-',label='Specific Collision Probability')
plt.legend(loc=4)
plt.xlabel('Simulation time [s]')
plt.ylabel('Collision probability [-]')
plt.title('Collision probability for N='+str(N)+' (deputy pos phi=0 theta=0)', size=12)

fig_number += 1
plt.figure(fig_number,figsize=(10,7.5))
plt.plot(aTSimulation_s,lPCollisionTByD[1][0][:],c='b',ls='-',label='Collision Probability')
plt.plot(aTSimulation_s,lPCollisionSpecTByD[1][0][:],c='r',ls='-',label='Specific Collision Probability')
plt.legend(loc=4)
plt.xlabel('Simulation time [s]')
plt.ylabel('Collision probability [-]')
plt.title('Collision probability for N='+str(N)+' (deputy pos phi=0 theta=90)', size=12)

fig_number += 1
plt.figure(fig_number,figsize=(10,7.5))
plt.plot(aTSimulation_s,lPCollisionTByD[2][0][:],c='b',ls='-',label='Collision Probability')
plt.plot(aTSimulation_s,lPCollisionSpecTByD[2][0][:],c='r',ls='-',label='Specific Collision Probability')
plt.legend(loc=4)
plt.xlabel('Simulation time [s]')
plt.ylabel('Collision probability [-]')
plt.title('Collision probability for N='+str(N)+' (deputy pos phi=90 theta=0)', size=12)

fig_number += 1
plt.figure(fig_number,figsize=(10,7.5))
plt.plot(aTSimulation_s,lPCollisionTByD[3][0][:],c='b',ls='-',label='Collision Probability')
plt.plot(aTSimulation_s,lPCollisionSpecTByD[3][0][:],c='r',ls='-',label='Specific Collision Probability')
plt.legend(loc=4)
plt.xlabel('Simulation time [s]')
plt.ylabel('Collision probability [-]')
plt.title('Collision probability for N='+str(N)+' (deputy pos phi=90 theta=90)', size=12)

#graficas segun k
#segun d (inicial) (para mayores)
#segun sigma
#segun N (para ver cuantas N están bien)

#probar alguna posición diferente del deputy [d,theta,phi]


#pc_profile(t0, tf, d_nom, D_nom, n_nom, q, sigma_vv_nom, N=N, dT=dT)
                
                
                
                
                
    