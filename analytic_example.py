from analytical import *
import matplotlib.pyplot as plt
import pandas as pd
import utilities as utl

N = 50
dT = 20


t0 = 0
tf = 3600 * 12
d = 150
D = 50
n = 4
k = 5
q = 1/k
sigma_vu = 0.005
sigma_vv = sigma_vu * q

k_list = [1, 2, 4, 5]

results_list = []
for k in k_list:
    q = 1/k
    xplot, yplot = pc_profile(t0, tf, d, D, n, q, sigma_vv, N=N, dT=dT)

    row_dict = {
        'T' : tf,
        'd' : d,
        'D' : D,
        'n' : n,
        'q' : q,
        'sigma_vv' : sigma_vv,
        'sigma_vu' : sigma_vu,
        'time' : xplot,
        'probability' : yplot
    }
    results_list.append(row_dict)

    plt.plot(xplot, yplot)

df = pd.DataFrame(data=results_list)
print(df)

utl.store(df, "analytic_example", "example")



plt.show()

#FOR DEBUG
# xplot, yplot = pc_profile(t0, tf, d, D, n, q, sigma_vv, Na=N, dT=dT,ptype='IP')
# plt.plot(xplot, yplot)
# plt.show()