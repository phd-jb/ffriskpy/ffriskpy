# -*- coding: utf-8 -*-

from mubody.mission import Mission
import numpy as np
import matplotlib.pyplot as plt
from astropy_healpix import HEALPix
from tqdm.auto import tqdm
import scipy as sc
import math


def grid(x, y, z, resX=100, resY=100):
    """
    Convierte 3 columnas de datos en un matplotlib grid

    Parameters
    ----------
    x : float
        Coordenada x de datos
    y : float
        Coordenada y de datos
    z : float
        Coordenada z de datos
    resX : int
        Número de datos resultantes en X para el matplotlib grid
    resY : int
        Número de datos resultantes en Y para el matplotlib grid
    
    Returns
    -------
    X : array(resX)
        Datos de coordenadas x repartidos en resX número de datos
    Y : array(resY)
        Datos de coordenadas y repartidos en resY número de datos
    Z : grid(resX,resY)
        Datos de coordenada z asignados a cada X e Y

    """
    xi = np.linspace(min(x), max(x), resX)
    yi = np.linspace(min(y), max(y), resY)
    Z = sc.interpolate.griddata((x, y), z, (xi[None,:], yi[:,None]), method='linear')
    X, Y = np.meshgrid(xi, yi)
    return X, Y, Z


#Generar orbita
#parametors
orb_rev = 1.2
mt = orb_rev*180*86400
n_seg = math.trunc(orb_rev*6)
ite = 4

#nombre
dir_name = 'case_1_2'

#control
generate = False

#Distancia inicial de referencia entre chief y deputy [m]
dr = 270.0

#Creación de objeto deputy de la clase Mission
deputy = Mission()

#Creación del objeto ChiefSat de la clase Mission
ChiefSat = Mission(mission_time=mt)
#Carga de propiedades de misión a partir del archivo 'chief_DCOrbit_12_2.0_10' de la carpeta 'Default_mission'
if generate:
    ChiefSat.OTM(N_segments=n_seg, opt_iterations=ite)
    ChiefSat.save("chief_OTM_Orbit_"+str(n_seg)+"_"+str(orb_rev)+"_"+str(ite), dir_name=dir_name)
else:
    ChiefSat.load("chief_OTM_Orbit_7_1.2_4", dir_name)

#Tiempo inicial de la órbita de la misión a partir de la cual se realizan analisis (1 rev = 15552000 s)
imt = 15552000.0/2.0

#Rango del tiempo de misión que se analiza (86400 (s for day) * 180 (days for rev) = 1 rev)
amt = np.float64(15552000.0)

#Tiempo a propagar el deputy para analisis [d]
admt = 20.0

#Numero de intervalos en que se divide la revolución a analizar
revorbINTV = 10

#Intervalo de tiempo en el que se van a analizar las diferencias entre trayectorias de chief y deputy [s]
dt = 1000

#Array de tiempo para analizar durante la propagación del deputy
time = np.linspace(0, admt*24.0*3600.0,dt)

#Contador de figuras
fig_number = 0

#Declaración de lista de vectores de estados iniciales del chief en cada punto de analisis de la revolución
aChiefState = []

#Array de tiempos correspondientes a la situación de los puntos iniciales a analizar sobre la órbita del chief
lmti = list(np.linspace(imt, imt+amt, revorbINTV+1))


#Definición de vectores unitarios a lo largo de la esfera para aplicar las velocidades iniciales de la distribución según la función HEALPix
hp = HEALPix(nside=4)
npix = hp.npix
pixels = np.arange(0, npix, 1)
#Extracción de componentes de los vectores segun las coordenadas latitud longitud aportadas por HEALPix
unvct = []
for i in range(0,len(pixels),1): 
    lon, lat = hp.healpix_to_lonlat([pixels[i]])
    vx = float(np.cos(lat) * np.cos(lon))
    vy = float(np.cos(lat) * np.sin(lon))
    vz = float(np.sin(lat))
    unvct.append([vx,vy,vz,np.degrees(lon),np.degrees(lat)])


#Media y desviación estandar de la distribución
mu, sigma = 0, 0.001
#Número de muestras en la distribución
N = 100


DataMean_pointlist = []
data_mean_rev = []

#Contador de puntos de orbita del chief sobre revolución
pto_orbit= 0

deputy.mt = admt*24.0*3600.0
#Bucle para analizar cada punto sobre la revolucion de la orbita del chief
for mti in lmti:
    print('----> Analyzing orbit in chief time: ' + str(mti) + ', rev: ' + str((mti-imt)/amt))
    aChiefState.append(ChiefSat.sat.s(mti))
            
    #Vector de estado (en posición) del punto de la orbita del chief analizado
    IC_chief = ChiefSat.sat.s(mti).reshape(-1,1)
    IC_frame = ChiefSat.frame
    
    data_mean_list = []
    data_mean = np.zeros(npix)
    
    data_mean_point = []
    pbar = tqdm(total=len(pixels))
    #Bucle para situar al deputy en cada punto de direcciones de la esfera sobre el chief (a distancia dr)
    for k in range(0,len(pixels),1):
        r0 = np.array([ unvct[k][0], unvct[k][1], unvct[k][2]])
        rv = dr * r0
        
        data_mean_posList = []
        data_mean_pos = np.zeros(npix)
        
        #Bucle para analizar cada vector unitario de la esfera de direcciones de velocidades a aplicar sobre el deputy
        for i in range(0,len(pixels),1):
            
            v0 = np.array([ unvct[i][0], unvct[i][1], unvct[i][2]])
            
            #Obtención de la distribución normal para aplicar diferentes velocidades sobre el deputy
            vmod_dist = np.random.normal(mu, sigma, N)
            data_counter = np.zeros(N)
            
            vmod_dist2 = []
            #Bucle para coger sólo las velocidades positivas de la distribución
            for v in range(0,len(vmod_dist),1):
                if vmod_dist[v] >= 0.0: vmod_dist2.append(vmod_dist[v])
            
            #Bucle para propagar el deputy para cada velocidad inicial positiva de la distribución
            for j in range(0,len(vmod_dist2),1):
                vv = v0 * vmod_dist2[j]
                #Vector de estado inicial de deputy (vector de estado del chief + offset)
                IC_deputy = IC_chief + np.concatenate([rv, vv]).reshape(-1,1)
                
                #Propagación del deputy para el tiempo mission_time
                deputy.IC(IC=IC_deputy, IC_frame=IC_frame, bar=False)
                
                #Calculo de la distancia en cada instante de tiempo (time) entre chief y deputy
                separation = np.linalg.norm(ChiefSat.sat.r(mti+time) - deputy.sat.r(time), axis=0)
                
                #Contabilizar si en la propagación la distancia es menor a 200 metros
                separation_flag = (np.where(separation<200, 1, 0).sum()>0)
                if separation_flag:
                    data_counter[j] = 1
                    
            #Guardado de resultados para cada dirección de velocidad inicial
            data_mean_posList.append([data_counter.mean()*100, unvct[i][3], unvct[i][4]])
            data_mean_pos[i] = data_counter.mean()
            

        #Guardado de resultados para cada posición diferente del deputy alrededor del chief
        data_mean_point.append([data_mean_pos.mean()*100, data_mean_posList])
        data_mean[k] = data_mean_pos.mean()
        data_mean_list.append([data_mean_pos.mean()*100, unvct[k][3], unvct[k][4]])
        pbar.update(1)
    
    pbar.close()
    #Guardado de resultados para cada posición inicial de analisis en la revolución del chief
    print(data_mean.mean() * 100, "%")
    data_mean_rev.append([data_mean.mean()*100, data_mean_list])
    
    
    
    #Grafico para mostrar cada probabilidad de colisión según posición del deputy respecto del chief (para una posición inicial del chief sobre su revolucion)
    fig_number += 1
    Xa, Ya, Za = grid([np.float64(x[3][0]) for x in unvct],[np.float64(x[4][0]) for x in unvct], [x[0] for x in data_mean_rev[pto_orbit][1][:]] )
    f6 = plt.figure(fig_number,figsize=(10,7.5))
    plt.contourf(Xa, Ya, Za, 20, cmap='RdBu_r')
    cbar = plt.colorbar()
    cbar.set_label(label='Probabilidad de colisión',size=12)
    plt.xlabel('Longitud [\u00B0]')
    plt.ylabel('Latitud [\u00B0]')
    plt.title('Probabilidad de colisión según la posición relativa inicial del deputy en '+str((mti-imt)/amt)+' rev', size=12)
    
    #Guardado de resultados de probabilidad de colisión en X e Y para cada posición inicial del chief en la revolución
    DataMean_pointlist.append(Za)
    
    pto_orbit += 1


#Grafico para mostrar la probabilidad de colisión a lo largo de toda la revolución del chief para deputy situado en stand-by
dataMean_plt = np.zeros((100,revorbINTV+1))
cnt = 0
for x in DataMean_pointlist:
    for i in range(0,100,1):
        dataMean_plt[i,cnt] = x[i,50]
    cnt += 1
fig_number += 1
f6 = plt.figure(fig_number,figsize=(10,7.5))
plt.contourf([(mti-imt)/amt for mti in lmti], Ya[:,0], dataMean_plt, 20, cmap='RdBu_r')
cbar = plt.colorbar()
cbar.set_label(label='Probabilidad de colisión',size=12)
plt.xlabel('Posición a lo largo de revolución [rev]')
plt.ylabel('Latitud [\u00B0]')
plt.title('Probabilidad de colisión según la posición relativa del deputy en longitud 180\u00B0 a lo largo de 1 revolución', size=12)


#Grafico para mostrar la probabilidad de colisión a lo largo de toda la revolución del chief para deputy situado en calibración
dataMean_plt = np.zeros((100,revorbINTV+1))
cnt = 0
for x in DataMean_pointlist:
    for i in range(0,100,1):
        dataMean_plt[i,cnt] = x[i,0]
    cnt += 1
fig_number += 1
f6 = plt.figure(fig_number,figsize=(10,7.5))
plt.contourf([(mti-imt)/amt for mti in lmti], Ya[:,0], dataMean_plt, 20, cmap='RdBu_r')
cbar = plt.colorbar()
cbar.set_label(label='Probabilidad de colisión',size=12)
plt.xlabel('Posición a lo largo de revolución [rev]')
plt.ylabel('Latitud [\u00B0]')
plt.title('Probabilidad de colisión según la posición relativa del deputy en longitud 0\u00B0 a lo largo de 1 revolución', size=12)
    