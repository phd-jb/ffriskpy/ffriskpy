import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import chi2
import pandas as pd
import scipy.stats as ss


#Media y desviación estandar de la distribución
mu, sigma = 0, 0.001
#Número de muestras en la distribución
N = 1000

#Obtención de la distribución normal para aplicar componentes de error en velocidad relativa
dvx = np.random.normal(mu, sigma, N)
dvy = np.random.normal(mu, sigma, N)
dvz = np.random.normal(mu, sigma, N)


fig, axs = plt.subplots(2)
fig.suptitle('Distribution comparison')

count, bins, ignore = axs[0].hist(dvx, 100, density=True)
axs[0].plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *
                   np.exp( - (bins - mu)**2 / (2 * sigma**2) ),
            linewidth=2, color='r')
axs[0].set_xlim(-3*sigma+mu, 3*sigma+mu)
axs[0].set_title('dvx/distNormal')
print("------ dv components distribution values:")
print("mu: "+str(mu))
print("sigma: "+str(sigma))


dvmod = np.zeros(N)
for i in range(0,N,1):
    dvmod[i] = np.linalg.norm(np.array([dvx[i],dvy[i],dvz[i]]))

#mu = np.mean(dvmod)
#sigma = np.std(dvmod)
df = dvmod
mu, sigma = ss.norm.fit(df)

count, bins, ignore = axs[1].hist(dvmod, 100, density=True)
axs[1].plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *
                  np.exp( - (bins - mu)**2 / (2 * sigma**2) ),
            linewidth=2, color='r')
axs[1].set_xlim(-3*sigma+mu, 3*sigma+mu)
axs[1].set_title('dvmod/distNormal')
print();print("------ dvmod distribution values:")
print("mu: "+str(mu))
print("sigma: "+str(sigma))


#https://blog.adrianistan.eu/estadistica-python-ajustar-datos-una-distribucion-parte-vii
##Prueba Kolmogorov-Smirnov
d, pvalor = ss.kstest(df,"norm",args=(mu, sigma))
print();print("------ Check Kolmogorov-Smirnov:")
print("d: "+str(d))
print("pvalor: "+str(pvalor))
# queremos confianza al 99%
if pvalor < 0.01:
    print("No se ajusta a una normal")
else:
    print("Puede ser que se ajuste a una normal, pero no se confirma")



#df = 3
#x = np.linspace(chi2.ppf(0.01, df), chi2.ppf(0.99, df), 100)
#x = bins
#rv = chi2(df)
#x = np.arange(0, 30, .05)
#axs[2].plot(x, chi2.pdf(x, df=20), color='r', lw=2)
#axs[2].plot(x, rv.pdf(x), 'k-', lw=2, label='frozen pdf')


